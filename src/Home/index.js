import React from 'react';
import Card from '@material-ui/core/Card';
import CardHeader from '@material-ui/core/CardHeader';
import CardContent from '@material-ui/core/CardContent';
import Typography from '@material-ui/core/Typography';
import Divider from '@material-ui/core/Divider';

export default () => (
  <Card>
    <CardHeader title="Home Card Header" subheader="May 14, 2018" />
    <Divider />
    <CardContent>
      <Typography component="span">
        Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nam maximus et risus vel dignissim. Duis bibendum
        aliquet metus, mattis condimentum nisi.
      </Typography>
      <Typography color="textSecondary">Aenean vitae nulla vel enim condimentum condimentum.</Typography>
      <Typography component="p">
        Curabitur molestie ultricies urna, in auctor neque pellentesque interdum. Proin a varius arcu, sit amet volutpat
        quam. Interdum et malesuada fames ac ante ipsum primis in faucibus. Maecenas ex tortor, hendrerit quis magna a,
        semper ullamcorper dui. Donec non eros dapibus ex lacinia tempor sit amet at massa. Vivamus consequat odio nec
        odio scelerisque aliquam. Duis fringilla, lorem a condimentum pretium, erat sapien bibendum mi, et malesuada
        odio turpis ut ligula. Vestibulum facilisis dolor vel dictum mollis.
      </Typography>
    </CardContent>
  </Card>
);
