const cyanColor = '#8be9fd';
const greyColor = '#282a36';
const greyLigtherColor = '#44475a';

export default {
  primary: cyanColor,
  grey: greyColor,
  background: {
    default: greyColor,
    paper: greyLigtherColor,
  },
};
