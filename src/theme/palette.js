import colors from './constants/colors';

export default {
  palette: {
    type: 'dark',
    primary: { main: colors.primary },
    grey: { main: colors.grey },
    background: {
      paper: colors.background.paper,
      default: colors.background.default,
    },
  },
};
