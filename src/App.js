import React from 'react';
import MuiThemeProvider from '@material-ui/core/styles/MuiThemeProvider';

import Main from './Main';
import theme from './theme';
import './App.css';

function App() {
  return (
    <MuiThemeProvider theme={theme}>
      <Main />
    </MuiThemeProvider>
  );
}

export default App;
