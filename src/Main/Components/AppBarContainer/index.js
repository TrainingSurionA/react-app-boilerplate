import React from 'react';
import PropTypes from 'prop-types';

import AppBar from '@material-ui/core/AppBar';
import classNames from 'classnames';
import IconButton from '@material-ui/core/IconButton';
import MenuIcon from '@material-ui/icons/Menu';
import Toolbar from '@material-ui/core/Toolbar';
import Typography from '@material-ui/core/Typography';

function AppBarContainer({ classes, isDrawerOpen, handleDrawerOpen }) {
  return (
    <AppBar position="absolute" className={classNames(classes.appBar, isDrawerOpen && classes.appBarShift)}>
      <Toolbar disableGutters={!isDrawerOpen}>
        <IconButton
          color="inherit"
          aria-label="open drawer"
          onClick={handleDrawerOpen}
          className={classNames(classes.menuButton, isDrawerOpen && classes.hide)}
        >
          <MenuIcon />
        </IconButton>
        <Typography variant="title" color="inherit" noWrap>
          React App
        </Typography>
      </Toolbar>
    </AppBar>
  );
}

AppBarContainer.propTypes = {
  classes: PropTypes.shape().isRequired,
  isDrawerOpen: PropTypes.bool.isRequired,
  handleDrawerOpen: PropTypes.func.isRequired,
};

export default AppBarContainer;
