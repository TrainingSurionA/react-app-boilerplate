// This file is shared across the demos.

import React, { Fragment } from 'react';
import { NavLink } from 'react-router-dom';
import ListItem from '@material-ui/core/ListItem';
import ListItemIcon from '@material-ui/core/ListItemIcon';
import ListItemText from '@material-ui/core/ListItemText';
import HomeIcon from '@material-ui/icons/Home';
import AcUnitIcon from '@material-ui/icons/AcUnit';
import AttachMoneyIcon from '@material-ui/icons/AttachMoney';

export const mainListItems = (
  <Fragment>
    <NavLink exact activeClassName="nav-selected" to="/">
      <ListItem button>
        <ListItemIcon>
          <HomeIcon />
        </ListItemIcon>
        <ListItemText primary="Home" />
      </ListItem>
    </NavLink>
    <NavLink activeClassName="nav-selected" to="/default-component">
      <ListItem button>
        <ListItemIcon>
          <AcUnitIcon />
        </ListItemIcon>
        <ListItemText primary="Default Component" />
      </ListItem>
    </NavLink>
  </Fragment>
);

export const otherListItems = (
  <Fragment>
    <NavLink activeClassName="nav-selected" to="/other-component">
      <ListItem button>
        <ListItemIcon>
          <AttachMoneyIcon />
        </ListItemIcon>
        <ListItemText primary="Other Component" />
      </ListItem>
    </NavLink>
  </Fragment>
);
