import React, { Component } from 'react';
import { BrowserRouter, Route } from 'react-router-dom';
import PropTypes from 'prop-types';
import { withStyles } from '@material-ui/core/styles';

import { AppBarContainer, DrawerContainer } from './Components';
import styles from './styles';
import Home from '../Home';
import DefaultComponent from '../DefaultComponent';
import OtherComponent from '../OtherComponent';

class Main extends Component {
  constructor(props) {
    super(props);
    this.state = { open: false };
    this.handleDrawerOpen = this.handleDrawerOpen.bind(this);
    this.handleDrawerClose = this.handleDrawerClose.bind(this);
  }

  handleDrawerOpen() {
    this.setState({ open: true });
  }

  handleDrawerClose() {
    this.setState({ open: false });
  }

  render() {
    const { classes, theme } = this.props;

    return (
      <BrowserRouter>
        <div className={classes.root}>
          <AppBarContainer classes={classes} isDrawerOpen={this.state.open} handleDrawerOpen={this.handleDrawerOpen} />
          <DrawerContainer
            classes={classes}
            theme={theme}
            isDrawerOpen={this.state.open}
            handleDrawerClose={this.handleDrawerClose}
          />

          <main className={classes.content}>
            <div className={classes.toolbar} />

            <div>
              <Route exact path="/" component={Home} />
              <Route path="/default-component" component={DefaultComponent} />
              <Route path="/other-component" component={OtherComponent} />
            </div>
          </main>
        </div>
      </BrowserRouter>
    );
  }
}

Main.propTypes = {
  classes: PropTypes.shape().isRequired,
  theme: PropTypes.shape().isRequired,
};

export default withStyles(styles, { withTheme: true })(Main);
